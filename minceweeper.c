#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <ncurses.h>


int size;


typedef struct square {
	bool mine;
	int row;
	int col;
	char symbol;
	int adjacent_mines;
	bool visible;
	struct square *neighbour[8];
} square;

/* https://stackoverflow.com/a/9278353 */
int read_int(char text[], int max) {
	int input;
	char line[256];
	while(true) {
		printf("%s [1-%d]: ", text, max);
		if (fgets(line, sizeof(line), stdin)) {
			if (1 == sscanf(line, "%d", &input)) {
				if(1 <= input && input <= max) {
					break;
				}
			}
		}
	}
	return input;
}

int reveal_square(square *s) {
	int i;
	int revealed = 0;
	if(s->visible == false) {
		revealed++;
		s->visible = true;
		if(s->adjacent_mines == 0) {
			for(i = 0; i < 8; i++) {
				revealed += reveal_square(s->neighbour[i]);
			}
		}
	}
	return revealed;
}

void reveal_board(square board[size + 2][size + 2]) {
	int r, c;
	for(r = 1; r <= size; r++) {
		for(c = 1; c <= size; c++) {
			board[r][c].visible = true;
		}
	}
}

void draw_board(square board[size + 2][size + 2]) {
	/* system("clear"); */
	int row, col;
	square *s;
	for(row = 1; row <= size; row++) {
		for(col = 1; col <= size; col++) {
			s = &board[row][col];
			if(s->visible) {
				printf("%c ", s->symbol);
			} else {
				printf("☐ ");
			}
		}
		printf("\n");
	}
}

int main(void) {
	srand(read_int("Input seed", 4096));
	size = read_int("Input board size", 40);
	square board[size + 2][size + 2];
	int i, row, col;
	int remaining_squares= 0;

	/* generate mine squares
	 *
	 * neighbour ptr array:
	 * 0 1 2
	 * 3 S 4
	 * 5 6 7
	 */
	square *s;
	for(row = 0; row < size + 2; row++) {
		for(col = 0; col < size + 2; col++) {
			s = &board[row][col];
			if(row == 0 || row == size + 1|| col == 0 || col == size + 1) {
				s->mine = false;
				s->visible = true;
				continue;
			}
			s->row = row;
			s->col = col;
			s->adjacent_mines = 0;
			s->mine = false;
			s->symbol = ' ';
			s->mine = rand() % 10 > 0 ? false : true;
			if(s->mine == false) {
				remaining_squares++;
			}
			s->visible = false;
			s->neighbour[0] = &board[row - 1][col - 1];
			s->neighbour[1] = &board[row - 1][col];
			s->neighbour[2] = &board[row - 1][col + 1];
			s->neighbour[3] = &board[row][col - 1];
			s->neighbour[4] = &board[row][col + 1];
			s->neighbour[5] = &board[row + 1][col - 1];
			s->neighbour[6] = &board[row + 1][col];
			s->neighbour[7] = &board[row + 1][col + 1];
		}
	}

	/* Find adjacent mines and set symbol */
	for(row = 1; row <= size; row++) {
		for(col = 1; col <= size; col++) {
			s = &board[row][col];
			if(s->mine) {
				s->symbol = '*';
				continue;
			}
			for(i = 0; i < 8; i++) {
				if(s->neighbour[i]->mine) {
					s->adjacent_mines++;
				}
			}
			if(s->adjacent_mines > 0) {
				s->symbol = s->adjacent_mines + 48;
			}
		}
	}

	draw_board(board);
	int r, c;
	while(true) {
		printf("\n");
		r = read_int("Input row", size);
		c = read_int("Input col", size);
		printf("\n");

		if(board[r][c].mine) {
			reveal_board(board);
			draw_board(board);
			printf("\nBoom, game over!\n");
			break;
		} else {
			remaining_squares -= reveal_square(&board[r][c]);
			if(remaining_squares == 0) {
				reveal_board(board);
				draw_board(board);
				printf("\nWow, you won!\n");
				break;
			} else {
				draw_board(board);
			}
		}
	}
	return 0;
}
