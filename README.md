# Minceweeper

A clone of a fairly obscure and unknown game.

## Textshot

```
$ build/minceweeper
Input seed [1-4096]: 100
Input board size [1-40]: 10
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐
☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐ ☐

Input row [1-10]: 5
Input col [1-10]: 5

☐ 2   1 ☐ ☐ ☐ ☐ ☐ ☐
☐ 2   1 ☐ ☐ ☐ ☐ ☐ ☐
1 1   1 1 2 1 2 ☐ ☐
              1 ☐ ☐
              2 ☐ ☐
              2 ☐ ☐
      1 1 1   1 2 ☐
1 1   2 ☐ 2     1 1
☐ 1   2 ☐ 2
☐ 1   1 ☐ 1
```
