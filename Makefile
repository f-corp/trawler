trawler: minceweeper.c
	mkdir -p build
	cc minceweeper.c -o build/minceweeper

clean:
	rm build/minceweeper
